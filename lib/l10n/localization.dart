import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

String userDescriptionText(
  BuildContext context, {
  required String gender,
  required String height,
  required String heightMeasurement,
  required String weight,
  required String weightMeasurement,
  required String calories,
}) {
  return AppLocalizations.of(context)!.userDescription(
    gender,
    height,
    heightMeasurement,
    weight,
    weightMeasurement,
    calories,
  );
}

String language(BuildContext context) {
  return AppLocalizations.of(context)!.language;
}

String save(BuildContext context) {
  return AppLocalizations.of(context)!.save;
}

String settings(BuildContext context) {
  return AppLocalizations.of(context)!.settings;
}

String disclaimerText(BuildContext context) {
  return AppLocalizations.of(context)!.disclaimer;
}

String understandText(BuildContext context) {
  return AppLocalizations.of(context)!.understand;
}

String weightMeasurement(BuildContext context, {required String measurement}) {
  return AppLocalizations.of(context)!.weightMeasurement(measurement);
}

String heightMeasurement(BuildContext context, {required String measurement}) {
  return AppLocalizations.of(context)!.heightMeasurement(measurement);
}

String week(BuildContext context) {
  return AppLocalizations.of(context)!.week;
}

String date(BuildContext context) {
  return AppLocalizations.of(context)!.date;
}

String weight(BuildContext context) {
  return AppLocalizations.of(context)!.weight;
}

String calories(BuildContext context) {
  return AppLocalizations.of(context)!.calories;
}

String deficit(BuildContext context) {
  return AppLocalizations.of(context)!.deficit;
}

String gender(BuildContext context) {
  return AppLocalizations.of(context)!.gender;
}

String height(BuildContext context) {
  return AppLocalizations.of(context)!.height;
}

String age(BuildContext context) {
  return AppLocalizations.of(context)!.age;
}

String submit(BuildContext context) {
  return AppLocalizations.of(context)!.submit;
}

String genderChoice(BuildContext context, {required String gender}) {
  return AppLocalizations.of(context)!.genderChoice(gender);
}

String activityText(BuildContext context, {required String activity}) {
  return AppLocalizations.of(context)!.activityText(activity);
}

// Navigation ----------------------------------------------------------
String home(BuildContext context) {
  return AppLocalizations.of(context)!.home;
}

String resultsButton(BuildContext context) {
  return AppLocalizations.of(context)!.resultsButton;
}

String weightLossTips(BuildContext context) {
  return AppLocalizations.of(context)!.weightLossTips;
}

// Calc Form ----------------------------------------------------------
String caloriesPerDay(BuildContext context) {
  return AppLocalizations.of(context)!.caloriesPerDay;
}

String howActive(BuildContext context) {
  return AppLocalizations.of(context)!.howActive;
}

String calcFormDisclaimer(BuildContext context) {
  return AppLocalizations.of(context)!.calcFormDisclaimer;
}

// Result Screen ----------------------------------------------------------
String resultScreenTitle(BuildContext context) {
  return AppLocalizations.of(context)!.resultTitle;
}

String resultsDisclaimer(BuildContext context) {
  return AppLocalizations.of(context)!.resultsDisclaimer;
}

String weightWithMeasure(BuildContext context,
    {required String weightMeasurement}) {
  return AppLocalizations.of(context)!.weightWithMeasure(weightMeasurement);
}

String caloriesUsed(BuildContext context) {
  return AppLocalizations.of(context)!.caloriesUsed;
}

// Result Info Scren ----------------------------------------------------------
String resultInfoTitle(BuildContext context) {
  return AppLocalizations.of(context)!.resultInfoTitle;
}

String bmiInfo(BuildContext context,
    {required String bmi, required int weightStatus}) {
  return AppLocalizations.of(context)!.bmiInfo(bmi, "$weightStatus");
}

String idealWeight(BuildContext context,
    {required String idealWeight, required String weightMeasurement}) {
  return AppLocalizations.of(context)!
      .idealWeight(idealWeight, weightMeasurement);
}

String idealWeightInfo(BuildContext context) {
  return AppLocalizations.of(context)!.idealWeightInfo;
}

String resultInfo(BuildContext context) {
  return AppLocalizations.of(context)!.resultInfo;
}

String moreInfo(BuildContext context) {
  return AppLocalizations.of(context)!.moreInfo;
}

String bmiRangeText(BuildContext context) {
  return AppLocalizations.of(context)!.bmiRangeText;
}

String tipsTitle(BuildContext context) {
  return AppLocalizations.of(context)!.tips_title;
}

String titleDisclaimer(BuildContext context) {
  return AppLocalizations.of(context)!.title_disclaimer;
}

String descDisclaimer(BuildContext context) {
  return AppLocalizations.of(context)!.desc_disclaimer;
}

String titleWeightloss(BuildContext context) {
  return AppLocalizations.of(context)!.title_weightloss;
}

String descWeightloss(BuildContext context) {
  return AppLocalizations.of(context)!.desc_weightloss;
}

String titleCalorie(BuildContext context) {
  return AppLocalizations.of(context)!.title_calorie;
}

String descCalorie(BuildContext context) {
  return AppLocalizations.of(context)!.desc_calorie;
}

String titleCalorieDeficit(BuildContext context) {
  return AppLocalizations.of(context)!.title_calorieDeficit;
}

String descCalorieDeficit(BuildContext context) {
  return AppLocalizations.of(context)!.desc_calorieDeficit;
}

String titleWork(BuildContext context) {
  return AppLocalizations.of(context)!.title_work;
}

String descWork(BuildContext context) {
  return AppLocalizations.of(context)!.desc_work;
}

String titleEat(BuildContext context) {
  return AppLocalizations.of(context)!.title_eat;
}

String descEat(BuildContext context) {
  return AppLocalizations.of(context)!.desc_eat;
}

String titleTraining(BuildContext context) {
  return AppLocalizations.of(context)!.title_training;
}

String descTraining(BuildContext context) {
  return AppLocalizations.of(context)!.desc_training;
}

String titleExercise(BuildContext context) {
  return AppLocalizations.of(context)!.title_exercise;
}

String descExercise(BuildContext context) {
  return AppLocalizations.of(context)!.desc_exercise;
}

String titleGaincontrol(BuildContext context) {
  return AppLocalizations.of(context)!.title_gaincontrol;
}

String descGaincontrol(BuildContext context) {
  return AppLocalizations.of(context)!.desc_gaincontrol;
}

String titleMakeitfun(BuildContext context) {
  return AppLocalizations.of(context)!.title_makeitfun;
}

String descMakeitfun(BuildContext context) {
  return AppLocalizations.of(context)!.desc_makeitfun;
}

String titleDontEatTooLittle(BuildContext context) {
  return AppLocalizations.of(context)!.title_dontEatTooLittle;
}

String descDontEatTooLittle(BuildContext context) {
  return AppLocalizations.of(context)!.desc_dontEatTooLittle;
}

String titleHowFastShouldILoseWeight(BuildContext context) {
  return AppLocalizations.of(context)!.title_how_fast_should_i_lose_weight;
}

String descHowFastShouldILoseWeight(BuildContext context) {
  return AppLocalizations.of(context)!.desc_how_fast_should_i_lose_weight;
}

String titleSurvivalMyth(BuildContext context) {
  return AppLocalizations.of(context)!.title_survival_myth;
}

String descSurvivalMyth(BuildContext context) {
  return AppLocalizations.of(context)!.desc_survival_myth;
}

String titleWeightDeficit(BuildContext context) {
  return AppLocalizations.of(context)!.title_weight_deficit;
}

String descWeightDeficit(BuildContext context) {
  return AppLocalizations.of(context)!.desc_weight_deficit;
}

String titleDontTrustScale(BuildContext context) {
  return AppLocalizations.of(context)!.title_dontTrustScale;
}

String descDontTrustScale(BuildContext context) {
  return AppLocalizations.of(context)!.desc_dontTrustScale;
}

String titleUsePhysicalMeasures(BuildContext context) {
  return AppLocalizations.of(context)!.title_usePhysicalMeasures;
}

String descUsePhysicalMeasures(BuildContext context) {
  return AppLocalizations.of(context)!.desc_usePhysicalMeasures;
}

String titleUsingScale(BuildContext context) {
  return AppLocalizations.of(context)!.title_usingScale;
}

String descUsingScale(BuildContext context) {
  return AppLocalizations.of(context)!.desc_usingScale;
}

String titleLowTrends(BuildContext context) {
  return AppLocalizations.of(context)!.title_lowTrends;
}

String descLowTrends(BuildContext context) {
  return AppLocalizations.of(context)!.desc_lowTrends;
}

String titleScheduling(BuildContext context) {
  return AppLocalizations.of(context)!.title_scheduling;
}

String descScheduling(BuildContext context) {
  return AppLocalizations.of(context)!.desc_scheduling;
}

String titleLooseSkin(BuildContext context) {
  return AppLocalizations.of(context)!.title_loose_skin;
}

String descLooseSkin(BuildContext context) {
  return AppLocalizations.of(context)!.desc_loose_skin;
}

String titleBmi(BuildContext context) {
  return AppLocalizations.of(context)!.title_bmi;
}

String descBmi(BuildContext context) {
  return AppLocalizations.of(context)!.desc_bmi;
}

String titleMaintaining(BuildContext context) {
  return AppLocalizations.of(context)!.title_maintaining;
}

String descMaintaining(BuildContext context) {
  return AppLocalizations.of(context)!.desc_maintaining;
}

String validNumber(BuildContext context) {
  return AppLocalizations.of(context)!.valid_number;
}

String noDataFound(BuildContext context) {
  return AppLocalizations.of(context)!.noDataFound;
}

String noDataAvailable(BuildContext context) {
  return AppLocalizations.of(context)!.noDataAvailable;
}

String goBack(BuildContext context) {
  return AppLocalizations.of(context)!.goBack;
}
