import 'package:flutter_riverpod/flutter_riverpod.dart';

final disclaimerProvider = StateProvider<Disclaimers>((ref) => Disclaimers());

class Disclaimers {
  bool showResultInfoDisclaimer = true;
}
