import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:weight_app/helpers/constants.dart';

final appSettingsProvider = StateProvider<AppSettings>((ref) => AppSettings(
      language: getLanguageFromLocale(
            WidgetsBinding.instance.platformDispatcher.locale.languageCode,
          ) ??
          Language.english,
    ));

class AppSettings {
  final Language language;
  AppSettings({this.language = Language.english});

  AppSettings copyWith({
    Language? language,
  }) {
    return AppSettings(
      language: language ?? this.language,
    );
  }
}
