import 'package:collection/collection.dart';

const double caloriePound = 3500;

enum Gender { male, female }

enum Measurement {
  imperial("in", "lbs"),
  metric("cm", "kg"),
  metricMeters("meters", "kg");

  final String heightText;
  final String weightText;
  const Measurement(this.heightText, this.weightText);
}

enum Activity {
  couchPotato(1.2, "0"),
  lightExercise(1.375, "1"),
  moderateExercise(1.55, "2"),
  activeExercise(1.725, "3"),
  athleticExercise(1.9, "4");

  final double points;
  final String val;
  const Activity(this.points, this.val);
}

enum Language {
  english("English", "en"),
  spanish("Español", "es");

  final String name;
  final String locale;
  const Language(this.name, this.locale);
}

Language? getLanguageFromLocale(String languageCode) {
  return Language.values.firstWhereOrNull(
    (element) => element.locale == languageCode,
  );
}

Language? getLanguageFromName(String name) {
  return Language.values.firstWhereOrNull(
    (element) => element.name == name,
  );
}
