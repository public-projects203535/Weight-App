import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:weight_app/helpers/navigation.dart' as routes;
import 'package:weight_app/providers/app_settings_provider.dart';
import 'package:weight_app/screens/home_screen.dart';
import 'package:weight_app/screens/result_info_screen.dart';
import 'package:weight_app/screens/result_screen.dart';
import 'package:weight_app/screens/settings_screen.dart';
import 'package:weight_app/screens/tips_screen.dart';
import 'package:window_manager/window_manager.dart';

import 'widgets/route_transitions/route_transitions.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (!kIsWeb && (Platform.isMacOS || Platform.isLinux || Platform.isWindows)) {
    await windowManager.ensureInitialized();
    WindowOptions windowOptions = const WindowOptions(
      minimumSize: Size(600, 700),
      center: true,
      backgroundColor: Colors.transparent,
      title: "Weight App",
    );
    windowManager.waitUntilReadyToShow(windowOptions, () async {
      await windowManager.show();
      await windowManager.focus();
    });
  }
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    AppSettings appSettings = ref.watch(appSettingsProvider);
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            backgroundColor: const Color.fromARGB(255, 99, 135, 152),
          ),
        ),
        pageTransitionsTheme: PageTransitionsTheme(
          builders: Map.fromIterable(
            TargetPlatform.values,
            key: (platform) => platform,
            value: (transition) {
              if (Platform.isIOS) {
                return const CupertinoPageTransitionsBuilder();
              } else if (Platform.isAndroid) {
                return const ZoomPageTransitionsBuilder();
              }
              return const NoTransitionsBuilder();
            },
          ),
        ),
      ),
      locale: Locale(appSettings.language.locale),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      builder: (context, child) => ResponsiveWrapper.builder(
        child,
        defaultScale: true,
        breakpoints: const [
          ResponsiveBreakpoint.resize(480, name: MOBILE),
          ResponsiveBreakpoint.autoScale(800, name: TABLET),
          ResponsiveBreakpoint.resize(1000, name: DESKTOP),
        ],
        background: Container(color: Theme.of(context).canvasColor),
      ),
      routes: {
        routes.homeScreen: (context) => const HomeScreen(),
        routes.resultScreen: (context) => const ResultScreen(),
        routes.resultInfoScreen: (context) => const ResultInfoScreen(),
        routes.tipsScreen: (context) => const TipsScreen(),
        routes.settingsScreen: (context) => const SettingsScreen(),
      },
      initialRoute: routes.homeScreen,
    );
  }
}
