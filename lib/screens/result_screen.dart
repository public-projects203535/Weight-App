import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:weight_app/helpers/constants.dart';
import 'package:weight_app/helpers/converter.dart' as converter;
import 'package:weight_app/helpers/simulator.dart' as simulator;
import 'package:weight_app/models/result_data.dart';
import 'package:weight_app/providers/disclaimer_provider.dart';
import 'package:weight_app/providers/form_data_provider.dart';
import 'package:weight_app/helpers/navigation.dart';
import 'package:weight_app/screens/result_info_screen.dart' as result_screen;
import 'package:weight_app/widgets/navigation/nav_drawer.dart';
import 'package:weight_app/widgets/navigation/side_navigator.dart';
import 'package:weight_app/widgets/result_screen/result_app_bar.dart';
import 'package:weight_app/l10n/localization.dart' as localization;

class ResultScreen extends ConsumerStatefulWidget {
  const ResultScreen({super.key});

  @override
  ConsumerState<ResultScreen> createState() => _ResultScreenState();
}

class _ResultScreenState extends ConsumerState<ResultScreen> {
  late final FormData? _formData;
  late final List<ResultData> _data;

  @override
  void initState() {
    super.initState();
    _formData = ref.read(formDataProvider);
    if (_formData == null) {
      // Show dialog after screen is built
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _showNoDataAlert();
      });
      return;
    }

    _data = simulator.simulateTwoYears(
      startingWeight: _formData!.weightData,
      activity: _formData!.activity,
      calorieIntake: _formData!.cals,
    );

    Disclaimers disclaimers = ref.read(disclaimerProvider);
    if (disclaimers.showResultInfoDisclaimer) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _showDisclaimer();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_formData == null) return const SizedBox.shrink();
    bool useMobile = Platform.isAndroid || Platform.isIOS;
    return Scaffold(
      body: Row(
        children: [
          if (!useMobile) const SideNavigator(),
          Expanded(
            child: Column(
              children: [
                ResultAppBar(
                  height: 45,
                  showBackButton: useMobile,
                  onQuestionTapped: () {
                    _showResultInfoScreen(!useMobile);
                  },
                ),
                Expanded(
                  child: _ResultChart(
                    data: _data,
                    weightMeasurement: _formData!.weightMeasurement,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      drawer: useMobile ? const NavDrawer() : null,
    );
  }

  void _showDisclaimer() {
    showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        actionsAlignment: MainAxisAlignment.center,
        contentPadding: const EdgeInsets.symmetric(horizontal: 12),
        titlePadding: const EdgeInsets.symmetric(vertical: 6),
        title: Text(
          localization.disclaimerText(context),
          textAlign: TextAlign.center,
        ),
        content: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 450),
          child: Text(
            localization.resultsDisclaimer(context),
            style: const TextStyle(fontSize: 18),
          ),
        ),
        actions: [
          TextButton(
            child: Text(
              localization.understandText(context),
              style: const TextStyle(fontSize: 18),
            ),
            onPressed: () {
              ref.read(disclaimerProvider).showResultInfoDisclaimer = false;
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  void _showResultInfoScreen(bool showAsPopup) {
    if (showAsPopup) {
      result_screen.showAsPopup(context);
      return;
    }
    Navigation.navigateTo(
      route: resultInfoScreen,
      context: context,
    );
  }

  void _showNoDataAlert() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text("Error"),
        content: Text(localization.noDataFound(context)),
        actions: [
          TextButton(
            onPressed: () => Navigation.popAndNavigateTo(
              route: homeScreen,
              context: context,
            ),
            child: const Text("OK"),
          ),
        ],
      ),
    );
  }
}

class _ResultChart extends StatelessWidget {
  final List<ResultData> data;
  final Measurement weightMeasurement;
  const _ResultChart({
    required this.data,
    required this.weightMeasurement,
  });

  @override
  Widget build(BuildContext context) {
    const TextStyle headerStyle = TextStyle(fontSize: 13);
    const TextStyle dataStyle = TextStyle(fontSize: 15);
    const double columnHeight = 62;
    return Column(
      children: [
        SizedBox(
          height: columnHeight,
          // Multi-row hack needed to keep Week text to the left while keeping everything else centered
          child: Row(
            children: [
              Expanded(
                flex: 2,
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        localization.week(context),
                        textAlign: TextAlign.center,
                        style: headerStyle,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        localization.date(context),
                        textAlign: TextAlign.center,
                        style: headerStyle,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        localization.weightWithMeasure(
                          context,
                          weightMeasurement: weightMeasurement.weightText,
                        ),
                        textAlign: TextAlign.center,
                        style: headerStyle,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                        localization.caloriesUsed(context),
                        textAlign: TextAlign.center,
                        style: headerStyle,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                        localization.deficit(context),
                        textAlign: TextAlign.center,
                        style: headerStyle,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              double weight = weightMeasurement == Measurement.imperial
                  ? converter.kgToPound(data[index].weight)
                  : data[index].weight;
              String date = DateFormat("MM/dd/yyyy").format(data[index].date);

              return Container(
                color: index.isEven ? Colors.grey.withOpacity(0.3) : null,
                child: SizedBox(
                  height: columnHeight,
                  child: Row(
                    children: [
                      // Once again multi-row hack needed to keep Week text to the left while keeping everything else centered
                      Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                "${data[index].week}",
                                textAlign: TextAlign.center,
                                style: dataStyle,
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Text(
                                date,
                                textAlign: TextAlign.center,
                                style: dataStyle,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                weight.toStringAsFixed(2),
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.yellow),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text(
                                data[index].calsUsed.toStringAsFixed(2),
                                textAlign: TextAlign.center,
                                style: dataStyle,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text(
                                data[index].deficit.toStringAsFixed(2),
                                textAlign: TextAlign.center,
                                style: dataStyle,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
