import 'dart:io';

import 'package:flutter/material.dart';
import "package:weight_app/l10n/localization.dart" as localization;
import 'package:weight_app/widgets/navigation/nav_drawer.dart';
import 'package:weight_app/widgets/navigation/side_navigator.dart';

//const double _tipSpacing = 6;

class TipsScreen extends StatelessWidget {
  const TipsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    ScrollController scrollController = ScrollController();
    bool useMobile = Platform.isAndroid || Platform.isIOS;
    final List<Widget> tips = [
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleDisclaimer(context),
        bodyText: localization.descDisclaimer(context),
        isExpanded: true,
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleWeightloss(context),
        bodyText: localization.descWeightloss(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleCalorie(context),
        bodyText: localization.descCalorie(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleCalorieDeficit(context),
        bodyText: localization.descCalorieDeficit(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleWork(context),
        bodyText: localization.descWork(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleEat(context),
        bodyText: localization.descEat(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleTraining(context),
        bodyText: localization.descTraining(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleExercise(context),
        bodyText: localization.descExercise(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleGaincontrol(context),
        bodyText: localization.descGaincontrol(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleMakeitfun(context),
        bodyText: localization.descMakeitfun(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleDontEatTooLittle(context),
        bodyText: localization.descDontEatTooLittle(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleHowFastShouldILoseWeight(context),
        bodyText: localization.descHowFastShouldILoseWeight(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleSurvivalMyth(context),
        bodyText: localization.descSurvivalMyth(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleWeightDeficit(context),
        bodyText: localization.descWeightDeficit(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleDontTrustScale(context),
        bodyText: localization.descDontTrustScale(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleUsePhysicalMeasures(context),
        bodyText: localization.descUsePhysicalMeasures(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleUsingScale(context),
        bodyText: localization.descUsingScale(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleLowTrends(context),
        bodyText: localization.descLowTrends(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleScheduling(context),
        bodyText: localization.descScheduling(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleLooseSkin(context),
        bodyText: localization.descLooseSkin(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleBmi(context),
        bodyText: localization.descBmi(context),
      ),
      const Divider(thickness: 2, height: 0),
      _TipTile(
        title: localization.titleMaintaining(context),
        bodyText: localization.descMaintaining(context),
      ),
    ];
    return Scaffold(
      appBar: useMobile
          ? AppBar(backgroundColor: Colors.transparent, elevation: 0)
          : null,
      body: WillPopScope(
        onWillPop: () async {
          Navigator.pop(context);
          return Future.value(false);
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (!useMobile) const SideNavigator(),
            Expanded(
              child: Scrollbar(
                thumbVisibility: true,
                controller: scrollController,
                child: SingleChildScrollView(
                  controller: scrollController,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Text(
                          localization.tipsTitle(context),
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 42,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const Icon(Icons.tips_and_updates,
                          color: Colors.amber, size: 80),
                      const SizedBox(height: 24),
                      ...tips,
                      const SizedBox(height: 24),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      drawer: useMobile ? const NavDrawer() : null,
    );
  }
}

class _TipTile extends StatefulWidget {
  final String title;
  final String bodyText;
  final bool isExpanded;
  const _TipTile({
    required this.title,
    required this.bodyText,
    this.isExpanded = false,
  });

  @override
  State<_TipTile> createState() => _TipTileState();
}

class _TipTileState extends State<_TipTile>
    with SingleTickerProviderStateMixin {
  final expansionTileKey = GlobalKey();
  late AnimationController _controller;
  late Animation<double> _iconTurns;

  bool _isExpanded = false;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200));
    _iconTurns = _controller.drive(Tween<double>(begin: 0, end: 0.5));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _handleTap(bool isExpanded) {
    setState(() {
      _isExpanded = isExpanded;
      if (isExpanded) {
        _controller.forward();
        expansionTileKey.ensureVisible();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      key: expansionTileKey,
      backgroundColor: Colors.white.withAlpha(22),
      iconColor: Colors.amberAccent,
      childrenPadding: const EdgeInsets.only(left: 10, right: 12, bottom: 8),
      trailing: const SizedBox.shrink(),
      onExpansionChanged: _handleTap,
      initiallyExpanded: widget.isExpanded,
      title: SizedBox(
        height: 75,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.max,
          children: [
            Flexible(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.title,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                    color: Colors.amber,
                  ),
                ),
              ),
            ),
            const SizedBox(width: 8),
            RotationTransition(
              turns: _iconTurns,
              child: Icon(
                Icons.expand_more,
                color: _isExpanded ? Colors.yellowAccent : null,
              ),
            ),
          ],
        ),
      ),
      children: [
        ConstrainedBox(
          constraints: const BoxConstraints(
            minHeight: 50,
            //  maxHeight: 600.0,
            maxWidth: 1400,
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 12),
            child: Align(
              alignment: Alignment.centerLeft,
              child: SelectableText(
                widget.bodyText,
                style: const TextStyle(
                  fontSize: 17,
                  letterSpacing: 0,
                  wordSpacing: 1,
                  height: 1.25,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

extension GlobalKeyEnsureVisible on GlobalKey {
  void ensureVisible() {
    final keyContext = currentContext;
    if (keyContext != null) {
      Future.delayed(const Duration(milliseconds: 200)).then((_) {
        Scrollable.ensureVisible(keyContext,
            duration: const Duration(milliseconds: 200));
      });
    }
  }
}
