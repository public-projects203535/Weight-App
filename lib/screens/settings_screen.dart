import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:weight_app/helpers/constants.dart';
import 'package:weight_app/providers/app_settings_provider.dart';
import 'package:weight_app/widgets/custom_drop_down.dart';
import 'package:weight_app/widgets/navigation/nav_drawer.dart';
import 'package:weight_app/widgets/navigation/side_navigator.dart';
import "package:weight_app/l10n/localization.dart" as localization;

// This screen is only visible for Desktop due to lack of other settings
// Language settings for mobile should be set via OS settings

class SettingsScreen extends ConsumerStatefulWidget {
  const SettingsScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends ConsumerState<SettingsScreen> {
  final _focusNode = FocusNode();
  final List<String> _languages = Language.values.map((e) => e.name).toList();
  String? _selectedLanguage;

  @override
  Widget build(BuildContext context) {
    final bool useMobile = Platform.isAndroid || Platform.isIOS;

    return Scaffold(
      appBar: useMobile
          ? AppBar(backgroundColor: Colors.transparent, elevation: 0)
          : null,
      body: WillPopScope(
        onWillPop: () async {
          Navigator.pop(context);
          return Future.value(false);
        },
        child: Row(
          children: [
            if (!useMobile) const SideNavigator(),
            Expanded(
              child: Center(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Localizations.override(
                      context: context,
                      locale: _selectedLanguage == null
                          ? null
                          : Locale(
                              getLanguageFromName(_selectedLanguage!)!.locale),
                      child: Builder(builder: (context) {
                        String saveButtonText = localization.save(context);
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${localization.language(context)}:",
                              style: const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(height: 16),
                            LanguageDropDown(
                              focusNode: _focusNode,
                              initialValue: _selectedLanguage ??
                                  getLocaleName(
                                    Localizations.localeOf(context)
                                        .languageCode,
                                  ),
                              languages: _languages,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedLanguage = newValue!;
                                });
                              },
                            ),
                            const SizedBox(height: 32),
                            Center(
                              child: ElevatedButton(
                                onPressed: () {
                                  _onSave(ref
                                      .read(appSettingsProvider.notifier)
                                      .update(_onSave));
                                },
                                style: ElevatedButton.styleFrom(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 32, vertical: 12),
                                  textStyle: const TextStyle(fontSize: 18),
                                ),
                                child: Text(saveButtonText),
                              ),
                            ),
                            // Since we only have a single setting, add some temporary spacing
                            const SizedBox(height: 500),
                          ],
                        );
                      }),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      drawer: useMobile ? const NavDrawer() : null,
    );
  }

  String? getLocaleName(String languageCode) {
    return getLanguageFromLocale(languageCode)?.name;
  }

  AppSettings _onSave(AppSettings appSettings) {
    if (_selectedLanguage != null) {
      return appSettings.copyWith(
        language: getLanguageFromName(_selectedLanguage!),
      );
    }
    return appSettings;
  }
}

class LanguageDropDown extends StatelessWidget {
  final FocusNode focusNode;
  final String? initialValue;
  final List<String> languages;
  final void Function(String?)? onChanged;
  const LanguageDropDown({
    super.key,
    required this.focusNode,
    required this.initialValue,
    required this.languages,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 400),
      child: CustomDropDown<String>(
        focusNode: focusNode,
        initialValue: initialValue,
        items: languages.map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        onChanged: onChanged,
      ),
    );
  }
}
