import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import "package:weight_app/l10n/localization.dart" as localization;
import 'package:weight_app/helpers/constants.dart';
import 'package:weight_app/helpers/converter.dart' as converter;
import 'package:weight_app/helpers/navigation.dart';
import 'package:weight_app/providers/form_data_provider.dart';

class ResultInfoScreen extends StatelessWidget {
  const ResultInfoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(localization.resultInfoTitle(context)),
        centerTitle: true,
      ),
      body: WillPopScope(
        onWillPop: () async {
          // Make the current route the result screen when pressing back
          Navigation.overrideCurrentRoute(resultScreen);
          Navigator.pop(context);
          return Future.value(false);
        },
        child: const _ResultInfoWidget(),
      ),
    );
  }
}

class _ResultInfoWidget extends ConsumerStatefulWidget {
  const _ResultInfoWidget();

  @override
  ConsumerState<_ResultInfoWidget> createState() => _ResultInfoWidgetState();
}

class _ResultInfoWidgetState extends ConsumerState<_ResultInfoWidget> {
  final ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    const TextStyle textStyle = TextStyle(fontSize: 18);
    const EdgeInsets textPadding =
        EdgeInsets.symmetric(horizontal: 8, vertical: 15);

    FormData? formData = ref.read(formDataProvider);

    return formData == null
        ? const NoDataWidget()
        : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Scrollbar(
                  thumbVisibility: true,
                  controller: _scrollController,
                  child: ListView(
                    controller: _scrollController,
                    children: [
                      Container(
                        padding: textPadding,
                        color: Colors.blueGrey.withOpacity(.4),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(_getUserDescription(context, formData),
                                style: textStyle),
                            const SizedBox(height: 5),
                            Text(_getBMIText(context, formData),
                                style: textStyle),
                          ],
                        ),
                      ),
                      Container(
                        padding: textPadding,
                        color: Colors.blueGrey.withOpacity(.15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(_getBMIRangeText(context), style: textStyle),
                            const SizedBox(height: 5),
                            Text(_getIdealWeightText(context, formData),
                                style: textStyle),
                            const SizedBox(height: 25),
                            Text(_getIdealWeightInfoText(context),
                                style: textStyle)
                          ],
                        ),
                      ),
                      Divider(color: Colors.grey[600], thickness: 2, height: 0),
                      Container(
                        padding: textPadding,
                        color: Colors.grey.withOpacity(.1),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(_getResultInfoText(context), style: textStyle),
                            const SizedBox(height: 5),
                            Text(
                              _getMoreInfoText(context),
                              style: textStyle,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          );
  }

  String _getUserDescription(BuildContext context, FormData data) {
    String gender = data.gender == Gender.male ? "male" : "female";
    String weightMeasurement = data.weightMeasurement.weightText;
    String weight = data.weight.toStringAsFixed(2);
    String heightMeasurement = data.heightMeasurement.heightText;
    String height = data.height.toStringAsFixed(2);

    return localization.userDescriptionText(
      context,
      gender: gender,
      height: height,
      heightMeasurement: heightMeasurement,
      weight: weight,
      weightMeasurement: weightMeasurement,
      calories: data.cals.toString(),
    );
  }

  String _getBMIText(BuildContext context, FormData data) {
    double bmi = data.weightData.bmi;
    int weightStatus = _getWeightStatus(bmi);

    return localization.bmiInfo(
      context,
      bmi: bmi.toStringAsFixed(2),
      weightStatus: weightStatus,
    );
  }

  String _getBMIRangeText(BuildContext context) {
    return localization.bmiRangeText(context);
  }

  String _getIdealWeightText(BuildContext context, FormData data) {
    String weightMeasurement = data.weightMeasurement.weightText;
    String idealWeight =
        _getWeight(data.weightData.idealWeight, data.weightMeasurement);

    return localization.idealWeight(
      context,
      idealWeight: idealWeight,
      weightMeasurement: weightMeasurement,
    );
  }

  String _getIdealWeightInfoText(BuildContext context) {
    return localization.idealWeightInfo(context);
  }

  String _getResultInfoText(BuildContext context) {
    return localization.resultInfo(context);
  }

  String _getMoreInfoText(BuildContext context) {
    return localization.moreInfo(context);
  }

  int _getWeightStatus(double bmi) {
    if (bmi >= 30) {
      return 0;
    } else if (bmi >= 24.9) {
      return 1;
    } else if (bmi < 18.5) {
      return 3;
    }
    return 2;
  }

  String _getWeight(double weight, Measurement weightMeasurement) {
    if (weightMeasurement == Measurement.imperial) {
      return converter.kgToPound(weight).toStringAsFixed(2);
    } else {
      return weight.toStringAsFixed(2);
    }
  }
}

class NoDataWidget extends StatelessWidget {
  const NoDataWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(localization.noDataAvailable(context)),
          const SizedBox(height: 10),
          ElevatedButton(
            onPressed: () => Navigation.popAndNavigateTo(
              route: homeScreen,
              context: context,
            ),
            style: ElevatedButton.styleFrom(
              backgroundColor: const Color.fromARGB(255, 99, 135, 152),
              minimumSize: const Size(100, 50),
            ),
            child: Text(localization.goBack(context)),
          )
        ],
      ),
    );
  }
}

void showAsPopup(BuildContext context) {
  showDialog<bool>(
    context: context,
    builder: (context) => Dialog(
      child: SizedBox(
        height: 475,
        width: 600,
        child: Stack(
          children: [
            const _ResultInfoWidget(),
            Positioned(
              right: 1,
              top: 1,
              child: GestureDetector(
                child: const Icon(Icons.close_rounded),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
