import 'package:flutter/material.dart';

class CustomDropDown<T> extends StatelessWidget {
  final FocusNode? focusNode;
  final T? initialValue;
  final List<DropdownMenuItem<T>> items;
  final List<Widget> Function(BuildContext)? selectedItemBuilder;
  final void Function(T?)? onChanged;
  final Widget? hint;
  final double? itemHeight;
  final Color? canvasColor;
  final Color? borderColor;
  final Color? iconColor;
  final Color? hoverColor;
  final TextStyle? textStyle;
  const CustomDropDown({
    super.key,
    required this.items,
    this.selectedItemBuilder,
    required this.onChanged,
    this.initialValue,
    this.focusNode,
    this.hint,
    this.itemHeight,
    this.canvasColor,
    this.borderColor,
    this.iconColor,
    this.hoverColor,
    this.textStyle,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: borderColor ?? Colors.grey),
        borderRadius: BorderRadius.circular(10),
        color: canvasColor,
      ),
      child: Focus(
        focusNode: focusNode,
        onFocusChange: (hasFocus) {
          if (focusNode != null && hasFocus) {
            focusNode!.unfocus(); // Move focus away when dropdown loses focus
          }
        },
        child: Theme(
          data: Theme.of(context).copyWith(
            hoverColor: hoverColor ?? Theme.of(context).hoverColor,
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<T>(
              isExpanded: true,
              value: initialValue,
              selectedItemBuilder: selectedItemBuilder,
              items: items,
              onChanged: onChanged,
              hint: hint,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              borderRadius: BorderRadius.circular(10),
              style: textStyle,
              itemHeight: itemHeight ?? kMinInteractiveDimension,
              iconEnabledColor: iconColor,
              dropdownColor: canvasColor ?? Theme.of(context).cardColor,
            ),
          ),
        ),
      ),
    );
  }
}
