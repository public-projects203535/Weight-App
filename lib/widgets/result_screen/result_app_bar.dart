import 'package:flutter/material.dart';
import 'package:weight_app/l10n/localization.dart';

class ResultAppBar extends StatelessWidget implements PreferredSizeWidget {
  final VoidCallback onQuestionTapped;
  final double height;
  final bool showBackButton;
  const ResultAppBar({
    super.key,
    required this.onQuestionTapped,
    this.height = 56,
    this.showBackButton = true,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(resultScreenTitle(context)),
      centerTitle: true,
      toolbarHeight: height,
      automaticallyImplyLeading: showBackButton,
      actions: [
        Padding(
          padding: const EdgeInsets.all(8),
          child: ClipOval(
            child: Material(
              color: Colors.amber.withOpacity(.85), // Button color
              child: InkWell(
                // Splash color
                onTap: onQuestionTapped,
                child: SizedBox(
                  width: height * .75,
                  child: const Icon(
                    Icons.question_mark_rounded,
                    size: 26,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(width: 6)
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
