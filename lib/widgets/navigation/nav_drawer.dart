import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:weight_app/helpers/navigation.dart';
import 'package:weight_app/providers/form_data_provider.dart';
import 'package:weight_app/widgets/navigation/footer.dart';
import 'package:weight_app/l10n/localization.dart' as localization;

class NavDrawer extends ConsumerWidget {
  const NavDrawer({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    bool isResultsDisabled = ref.read(formDataProvider) == null;
    return Drawer(
      width: 300,
      backgroundColor: const Color.fromARGB(255, 55, 55, 55),
      child: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                SizedBox(
                  height: 120,
                  child: DrawerHeader(
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      border: Border(
                        bottom: Divider.createBorderSide(context,
                            color: Colors.transparent, width: 0),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        'Weight App',
                        style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context)
                              .textTheme
                              .titleLarge!
                              .color!
                              .withOpacity(.85),
                        ),
                      ),
                    ),
                  ),
                ),
                _NavButton(
                  icon: Icons.home,
                  text: localization.home(context),
                  isSelected: Navigation.isHomeScreen,
                  onPressed: Navigation.isHomeScreen
                      ? null
                      : () => Navigation.popAndNavigateTo(
                          route: homeScreen, context: context),
                ),
                _NavButton(
                  icon: Icons.analytics_outlined,
                  text: localization.resultsButton(context),
                  isSelected: Navigation.isResultScreen,
                  isDisabled: isResultsDisabled,
                  onPressed: isResultsDisabled
                      ? _showToast
                      : Navigation.isResultScreen
                          ? null
                          : () => Navigation.popAndNavigateTo(
                              route: resultScreen, context: context),
                ),
                _NavButton(
                  icon: Icons.tips_and_updates,
                  text: localization.weightLossTips(context),
                  isSelected: Navigation.isTipsScreen,
                  onPressed: Navigation.isTipsScreen
                      ? null
                      : () => Navigation.popAndNavigateTo(
                          route: tipsScreen, context: context),
                ),
              ],
            ),
          ),
          const Footer(),
        ],
      ),
    );
  }

  void _showToast() {
    Fluttertoast.showToast(
      msg: 'Please fill out the form first!',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 3,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }
}

class _NavButton extends StatelessWidget {
  final IconData icon;
  final String text;
  final bool isSelected;
  final bool isDisabled;
  final VoidCallback? onPressed;
  const _NavButton(
      {required this.icon,
      required this.text,
      required this.onPressed,
      this.isSelected = false,
      this.isDisabled = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      color: isSelected ? Colors.grey.withOpacity(.6) : null,
      height: 50,
      margin: const EdgeInsetsDirectional.only(bottom: 5),
      //padding: const EdgeInsets.only(left: 6),
      child: InkWell(
        onTap: onPressed,
        child: Row(
          children: [
            const SizedBox(width: 20),
            Icon(
              icon,
              size: 24,
              color: isDisabled
                  ? Colors.white24
                  : isSelected
                      ? Colors.white.withOpacity(onPressed == null ? .6 : .9)
                      : Colors.white.withOpacity(onPressed == null ? .15 : .9),
            ),
            const SizedBox(width: 5),
            Text(
              text,
              style: TextStyle(
                color: isDisabled
                    ? Colors.white24
                    : isSelected
                        ? Colors.white.withOpacity(onPressed == null ? .6 : .9)
                        : Colors.white
                            .withOpacity(onPressed == null ? .15 : .9),
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            )
          ],
        ),
      ),
    );
  }
}
