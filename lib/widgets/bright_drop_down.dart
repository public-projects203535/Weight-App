import 'package:flutter/material.dart';
import 'package:weight_app/widgets/custom_drop_down.dart';

class BrightDropDown<T> extends StatelessWidget {
  final FocusNode? focusNode;
  final T? initialValue;
  final List<DropdownMenuItem<T>> items;
  final List<Widget> Function(BuildContext)? selectedItemBuilder;
  final void Function(T?)? onChanged;
  final Widget? hint;
  final double? fontSize;
  final double? itemHeight;
  const BrightDropDown({
    super.key,
    this.focusNode,
    this.initialValue,
    required this.items,
    this.selectedItemBuilder,
    this.onChanged,
    this.hint,
    this.fontSize,
    this.itemHeight,
  });

  @override
  Widget build(BuildContext context) {
    return CustomDropDown(
      initialValue: initialValue,
      focusNode: focusNode,
      selectedItemBuilder: selectedItemBuilder,
      items: items,
      onChanged: onChanged,
      canvasColor: Colors.grey[350],
      borderColor: Theme.of(context).canvasColor.withOpacity(0.5),
      iconColor: Colors.black,
      hoverColor: Theme.of(context).canvasColor.withOpacity(.2),
      textStyle: TextStyle(color: Colors.black, fontSize: fontSize),
      itemHeight: itemHeight,
    );
  }
}
