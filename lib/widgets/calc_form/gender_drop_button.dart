import 'package:flutter/material.dart';
import 'package:weight_app/helpers/constants.dart';
import 'package:weight_app/l10n/localization.dart' as localization;
import 'package:weight_app/widgets/bright_drop_down.dart';

class GenderDropButton extends StatefulWidget {
  final Gender? initialValue;
  final Function(Gender) onChanged;
  const GenderDropButton(
      {super.key, required this.onChanged, this.initialValue});

  @override
  State<GenderDropButton> createState() => _GenderDropButtonState();
}

class _GenderDropButtonState extends State<GenderDropButton> {
  final FocusNode _focusNode = FocusNode();
  late Gender _dropdownValue;
  @override
  void initState() {
    super.initState();
    _dropdownValue = widget.initialValue ?? Gender.male;
  }

  @override
  Widget build(BuildContext context) {
    return BrightDropDown(
      initialValue: _dropdownValue,
      focusNode: _focusNode,
      selectedItemBuilder: (context) {
        return Gender.values
            .map((gender) => Container(
                  alignment: Alignment.center,
                  width: 100,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      localization.genderChoice(context, gender: gender.name),
                    ),
                  ),
                ))
            .toList();
      },
      items: Gender.values.map<DropdownMenuItem<Gender>>(
        (Gender value) {
          return DropdownMenuItem<Gender>(
            value: value,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                localization.genderChoice(context, gender: value.name),
              ),
            ),
          );
        },
      ).toList(),
      onChanged: (Gender? value) {
        setState(() {
          _dropdownValue = value!;
        });
        widget.onChanged(value!);
      },
      fontSize: 20,
    );
  }
}
