import 'package:flutter/material.dart';
import 'package:weight_app/helpers/constants.dart';
import 'package:weight_app/l10n/localization.dart' as localization;
import 'package:weight_app/widgets/bright_drop_down.dart';

class ActivityDropButton extends StatefulWidget {
  final Activity? initialValue;
  final Function(Activity) onChanged;
  const ActivityDropButton(
      {super.key, required this.onChanged, this.initialValue});

  @override
  State<ActivityDropButton> createState() => _ActivityDropButtonState();
}

class _ActivityDropButtonState extends State<ActivityDropButton> {
  final FocusNode _focusNode = FocusNode();
  late Activity _dropdownValue;
  @override
  void initState() {
    super.initState();
    _dropdownValue = widget.initialValue ?? Activity.lightExercise;
  }

  @override
  Widget build(BuildContext context) {
    return BrightDropDown(
      focusNode: _focusNode,
      initialValue: _dropdownValue,
      itemHeight: 70,
      selectedItemBuilder: (context) {
        return Activity.values
            .map((value) => Center(
                  child: Text(
                    localization.activityText(context, activity: value.val),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 18),
                  ),
                ))
            .toList();
      },
      items: Activity.values.map<DropdownMenuItem<Activity>>(
        (Activity value) {
          return DropdownMenuItem<Activity>(
            value: value,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 2),
              child: Text(
                localization.activityText(context, activity: value.val),
                style: const TextStyle(fontSize: 18),
              ),
            ),
          );
        },
      ).toList(),
      onChanged: (Activity? value) {
        setState(() {
          _dropdownValue = value!;
        });
        widget.onChanged(value!);
      },
    );
  }
}
