import 'package:flutter/material.dart';
import 'package:weight_app/helpers/constants.dart';
import 'package:weight_app/l10n/localization.dart' as localization;
import 'package:weight_app/widgets/bright_drop_down.dart';

class MeasurementDropButton extends StatefulWidget {
  final Measurement? initialValue;
  final bool isWeight;
  final Function(Measurement) onChanged;
  const MeasurementDropButton({
    super.key,
    required this.isWeight,
    required this.onChanged,
    this.initialValue,
  });

  @override
  State<MeasurementDropButton> createState() => _MeasurementDropButtonState();
}

class _MeasurementDropButtonState extends State<MeasurementDropButton> {
  final FocusNode _focusNode = FocusNode();
  late Measurement _dropdownValue;
  @override
  void initState() {
    super.initState();
    _dropdownValue = widget.initialValue ?? Measurement.imperial;
  }

  @override
  Widget build(BuildContext context) {
    final Set<String> usedMeasurements = {};
    return BrightDropDown(
      initialValue: _dropdownValue,
      focusNode: _focusNode,
      selectedItemBuilder: (context) {
        return Measurement.values.map((value) {
          String text = widget.isWeight
              ? localization.weightMeasurement(context,
                  measurement: value.weightText)
              : localization.heightMeasurement(context,
                  measurement: value.heightText);
          return Container(
            alignment: Alignment.center,
            width: 100,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                text,
                textAlign: TextAlign.end,
              ),
            ),
          );
        }).toList();
      },
      items: Measurement.values.expand<DropdownMenuItem<Measurement>>(
        (Measurement value) {
          String text = widget.isWeight
              ? localization.weightMeasurement(context,
                  measurement: value.weightText)
              : localization.heightMeasurement(context,
                  measurement: value.heightText);

          if (usedMeasurements.contains(text)) {
            return []; // return empty list if the measurement is already used
          }

          usedMeasurements.add(text);

          return [
            DropdownMenuItem<Measurement>(
              value: value,
              child: Text(text),
            )
          ];
        },
      ).toList(),
      onChanged: (Measurement? value) {
        setState(() {
          _dropdownValue = value!;
        });
        widget.onChanged(value!);
      },
      fontSize: 20,
    );
  }
}
