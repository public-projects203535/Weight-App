Flutter project to help aid in tracking weight loss, and understanding weight loss.

This project uses the Mifflin-St Jeor equation to calculate BMR

Project is still a WIP

# Warning
This application, it's contents, and the contents of the source code are not medical advice. Please consult with a medical professional instead.

# Requirements

Flutter 3.13+

# Setup

Run the `flutter pub get` command to fech and update packages

Then run the `flutter gen-l10n` command to generate localization files